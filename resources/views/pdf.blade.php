<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF</title>
    <style rel="stylesheet">
        @font-face {
            font-family: 'zawgyi';
            src: url({{ storage_path('fonts\ZawgyiOne.ttf') }}) format("truetype");
        }
        
        body {
            font-family: 'zawgyi';
        }

        .wrapper {
            width: 100%;
            padding: 20px;
        }

        .wrapper h1 {
            text-align: center;
            font-family: 'zawgyi';
        }
        
        table tr td {
            font-family: 'zawgyi';
        }

    </style>
</head>
<body>

    <div class="wrapper">
        <table>
            <tr>
                <td>{{ $mgmg }}</td>
                <td>{{ $kk }}</td>
            </tr>
        </table>
    </div>
    
</body>
</html>
